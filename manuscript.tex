%\RequirePackage{fix-cm}
%
%\documentclass[preprint,draft,10pt]{elsarticle}
\documentclass[preprint,10pt]{elsarticle}
%
\synctex=1 % for forward and inverse search
%$\phi_{\rm e,R-D}-\phi_{\rm e,R-E}$
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
%\graphicspath{{./figures/}}
\usepackage[font=small]{caption}
%
%\usepackage{mathptmx}      % use Times fonts if available on your TeX system
%
% insert here the call for the packages your document requires
\usepackage[colorlinks,bookmarksopen,bookmarksnumbered,citecolor=blue,urlcolor=blue]{hyperref}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{latexsym}
\usepackage{bm}
\usepackage{multirow}
\usepackage{rotating}
\usepackage{geometry}
\usepackage{csquotes}
\usepackage[usenames, dvipsnames]{color}
\usepackage[per-mode=reciprocal]{siunitx} % typeset units
\DeclareSIUnit\Molar{\textsc{m}}


% please place your own definitions here and don't use \def but
\newcommand{\tensor}[1]{\mathbf{#1}}
\newcommand{\symbtensor}[1]{\boldsymbol{#1}}
\renewcommand{\vec}[1]{\mathbf{#1}}
\newcommand{\symbvec}[1]{\boldsymbol{#1}}
\newcommand{\Rvec}[1]{\underline{#1}} % vector of coefficients
\newcommand{\Rvecb}[1]{\underline{\vec{#1}}} % vector of coefficients
%\newcommand{\Grad}{\operatorname{Grad}} % vector of coefficients
\newcommand{\Grad}{\nabla} % vector of coefficients
\newcommand{\dd}{\,\mathrm{d}}
\newcommand{\Cbf}{{\bf C}}
\newcommand{\Fbf}{{\bf F}}
\newcommand{\sigbf}{\mbox{\boldsymbol {$\sigma$}}}
\newcommand{\fbf}{{\bf f}}
\newcommand{\fbft}{\hat{\fbf}}
\newcommand{\ubf}{{\bf u}}
\newcommand{\Div}{\operatorname{Div}}

% \title{Computationally efficient modeling of intracardiac electrograms using a reaction-eikonal (R-E) model}
% \author{Neic, A. \and Campos, F.O. \and Prassl, A.J. \and Bishop, M.J. \and Vigmond, E.J. \and Plank, G.}

\begin{document}

\newgeometry{left=1.2in,right=1.2in}
%
\begin{frontmatter}

\title{Finite element modeling of fibrotic clefts in the heart using Robin-type boundary conditions}
\tnotetext[t1]{This research was supported by the grants F3210-N18 and I2760-B30
       from the Austrian Science Fund (FWF) and the EU grant CardioProof 611232.
       We acknowledge PRACE for awarding us access to resource SuperMUC based in Germany at LRZ (grant CAMEL),
       and the Vienna Scientific Cluster VSC3.
       Further support was received from ERACoSysMed aministered through ANR-15-CMED-0003-01
       and Agence National de Recherche grant ANR-13-MONU-0004-02.
       }
%
%

\author[MUG]{Aurel Neic}
\author[MUG]{Caroline Costa}
%\author[KCL]{Steven A. Niederer}
%\author[KCL]{Martin J. Bishop}
\author[BDX]{Edward J. Vigmond}
\author[MUG]{Gernot Plank\corref{cor}}
\ead{gernot.plank@medunigraz.at}

\cortext[cor]{Corresponding author}


%\dedication{\small This research was supported by the grants F3210-N18
%   from the Austrian Science Fund (FWF), NIH 1RO1 HL 10119601l and the
%   EU grant CardioProof 611232.
%   We acknowledge PRACE for awarding us access to resource SuperMUC based in Germany at LRZ (grant CAMEL),
%   and, partially, ARCHER based in the UK at EPCC (project e348).}

\address[MUG]{Institute of Biophysics, Medical University of Graz,
         Graz, Austria}
\address[DHZ]{Dept. of Congenital Heart Diseases and Pediatric Cardiology, German Heart Institute
         Berlin, Berlin, Germany}
\address[KCL]{Dept. Biomedical Engineering, Division of Imaging Sciences and Biomedical Engineering,
         King's College of London, London, United Kingdom}
\address[BDX]{University of Bordeaux, Bordeaux, France}


\begin{abstract}


\end{abstract}


\begin{keyword}
Cardiac Electrophysiology, Bidomain Model, Fibrosis.
\end{keyword}
\end{frontmatter}


\section{Introduction}
\label{sec:intro}



\section{Methods}
\label{sec:methods}



\subsection{Modeling myocyte-fibroblast interaction}
\label{sec:robin}
Fibrosis is typically modelled as an insulating layer which interrupts the intracellular matrix, that is, 
electrical propagation is blocked across layers of fibrotic tissue \cite{Costa2014,Campos2013,Campos2010}.
Cardiac fibroblasts are a type of cell that produces a variety of extracellular matrix components,
including multiple collagens \cite{Souders2009}. 
There is growing evidence suggesting that these cells may express gap junctions and connect to myocytes, 
thus recoupling the intracellular matrix \cite{Tashalee2015,Nguyen2014}. In this scenario, propagation is partially reestablished
across the insulating layers. However, propagation at these sites is slow compared to propagation in the normal myocardium
and depends on the potential difference across the fibrotic layers.
To model this scenario, an extended discontinuous finite element approach (XdFE) was derived, 
where instead of no flux, a resistive flux is enforced across the artificial boundary layers,
created via nodal renumbering.

To derive the modified finite element formulation, we consider the FE discretization presented in Sec. \ref{sec:FEM}, 
where no-flux boundary conditions are enforced at the boundary of the domain (Equation \ref{eq:noflux}).
The new formulation is demonstrated considering a discretization of a domain $\Omega$ into tetrahedral elements, thus, the outer surface, $\boldsymbol{\Gamma}$, 
consists of a discrete triangular surface.
In this context, an inner surface of ${\Omega}$ is selected, ${\Gamma_0}$, as shown in Fig \ref{fig:domain_split}-A). 
The nodes in ${\Gamma_0}$ are then renumbered, as in the discontinuous finite element approach (Chapter 4), thus,
creating a new co-located surface, ${\Gamma_1}$, as shown in Fig \ref{fig:domain_split}-B).
Here, ${\Gamma_0}$ defines the original nodes of the interface and ${\Gamma_1}$ holds the new set of nodes.
From now on, the original set of nodes on ${\Gamma_0}$ is defined as $I_0$ and the new 
set of nodes on ${\Gamma_1}$, as $I_1$. 
$I_1$ is added after the end of the original set of nodes of the domain $I = 0..N$,
where $N$ is the total number of nodes originally on ${\Omega}$,
that is, $I_1 = N+1..N_s$, with $N_s = N + N_{split-nodes}$.
It is important to mention that, there is a 1:1 relation between the two sets $I_0$ and $I_1$, that is, 
each node in $I_0$ corresponds to exactly one node in $I_1$.

\begin{figure}[h]
	\centering
	\includegraphics{figures/robinBC_domain_split.png}
	% domain_split.png: 1399x522 pixel, 300dpi, 11.84x4.42 cm, bb=0 0 336 125
	\caption{A generic domain $\Omega$. A) Showing the marked surface ${\Gamma_0}$ inside the domain ${\Omega}$ and the original
		outer surface $\Gamma$. B) After single-sided nodal splitting, a new co-located surface ${\Gamma_1}$ is created.}
	\label{fig:domain_split}
\end{figure}

The effect of the single-sided nodal renumbering is that the surfaces ${\Gamma_0}$ and ${\Gamma_1}$ are treated as part of 
the outer surface ${\Gamma}$, that is, a new outer surface ${\Gamma'}$ is created which is defined as ${\Gamma' = \Gamma + \Gamma_0 + \Gamma_1}$. Thus, no-flux is enforced across the entire ${\Gamma'}$.

The aim of the new approach is to enforce a resistive flux instead of no flux across ${\Gamma'}$.
Therefore, the insulating gap is recoupled by assuming a resistive infinitely thin barrier between 
the corresponding triangles on ${\Gamma_0}$ and ${\Gamma_1}$.
The flux out of triangle 0 is equal to the flux into triangle 1 
and the flux is proportional to the potential difference across the gap. That is, we have in discrete weak form
\begin{equation}
\int_{{\Gamma_0}} \boldsymbol{\sigma} \nabla \Phi^h \varphi_j d{\Gamma_0} 
= -\int_{{\Gamma_1}} \boldsymbol{\sigma} \nabla \Phi^h \varphi_j d{\Gamma_1},
\end{equation}
where $\varphi_j$ are the basis functions defined by Equation \ref{eq:basis_function}.

When computing the driving potential difference across the gap for a given side 0 or 1, projection operations are necessary.
Thus, two projection operators are defined, $\mathbf{P_{1 \mapsto 0}}$ and $\mathbf{P_{0 \mapsto 1}}$,
which project (or shift) indices from 1 to 0 and from 0 to 1, respectively, that is
\begin{eqnarray}
{P_{0 \rightarrow 1}} = {\Gamma_0 \rightarrow \Gamma_1} \\
{P_{1 \rightarrow 0}} = {\Gamma_1 \rightarrow \Gamma_0}
\end{eqnarray}

Thus, the contribution of the boundary ${\Gamma_0}$ is given by
\begin{equation}
\int_{{\Gamma_0}} \boldsymbol{\sigma} \nabla \Phi^h \varphi_j d{\Gamma_0} 
= \alpha \int_{{\Gamma_0}} \varphi_j \nabla \Phi^h \cdot \boldsymbol{n} d{\Gamma_0}
\approx \alpha \int_{{\Gamma_0}} \varphi_j ({P_{1 \rightarrow 0}} \Phi^h |_{{\Gamma_1}} 
- \Phi^h |_{{\Gamma_0}}) d{\Gamma},
\end{equation}
where $\alpha$ is the scalar conductivity across the boundary layer.
%
And the contribution of the boundary ${\Gamma_1}$ is given by
\begin{equation}
\int_{{\Gamma_1}} \boldsymbol{\sigma} \nabla \Phi^h \varphi_j d{\Gamma_1} 
= \alpha \int_{{\Gamma_1}} \varphi_j \nabla \Phi^h \cdot \boldsymbol{n} d{\Gamma_1}
\approx \alpha \int_{{\Gamma_1}} \varphi_j ({P_{0 \rightarrow 1}} \Phi^h |_{{\Gamma_0}} 
- \Phi^h |_{{\Gamma_1}}) d{\Gamma}
\end{equation}

This new formulation leads to a new global matrix which is is defined as
\begin{eqnarray}
{B[i,j]} = \begin{cases}
{-M_{{\Gamma_0}}},                     & if (i,j) \in I_0 \times I_0  \nonumber \\ 
{-P_{1 \rightarrow 0} M_{{\Gamma_1}}}, & if (i,j) \in I_0 \times I_1  \nonumber \\
{P_{0 \rightarrow 1} M_{{\Gamma_0}}},  & if (i,j) \in I_1 \times I_0  \nonumber \\
{-M_{{\Gamma_1}}},                     & if (i,j) \in I_1 \times I_1  \nonumber \\
0,                                                       & else                 
\end{cases}
\end{eqnarray}

Where 
\begin{align}
{M_{{\Gamma_0}}} &= \int_{{\Gamma_0}} \varphi_j \varphi_k d{\Gamma_0},~j,k \in I_0 \\
{M_{{\Gamma_1}}} &= \int_{{\Gamma_1}} \varphi_j \varphi_k d{\Gamma_1},~j,k \in I_1
\end{align}
are the global boundary mass matrices. 

Thus, for the monodomain model, the new discrete formulation is in matrix form:
\begin{equation}
{M_i} I_m = - {K_i'} V_m,
\end{equation}
where $K_i' = K_i + B$.

And for the bidomain model, the new discrete formulation is:
\begin{eqnarray}
({K_i'} + {K_e}) \Phi_e &=& K_i' V_m \\ \nonumber
{M_i} I_m &=& - K_i' V_m
\end{eqnarray}

However, since in the bidomain model the boundary layer
is created only in $\Omega_i$, the matrices $K_i'$ and ${K_e}$ have different sizes. 
${K_e}$ is a $N\times N$ matrix, where $N$ is the original number of nodes of the system, and ${K_i'}$ is a $N_s\times N_s$ 
matrix.

To solve this problem, ${K_e}$ is extended to $N_s\times N_s$. To assure that ${K_e}$ is continuous, the matrix is assembled 
using the original set of nodes in ${\Omega_e}$, $I_e = 1..N$. To enforce continuity across the 
boundary layer in $\Phi_e$, the following equation is added to system

\begin{equation}
\Phi_{e,i} = \Phi_{e,j},
\end{equation}
where the index $i$ belongs to ${\Gamma_0}$ and $j$ is corresponding node in ${\Gamma_1}$.
This equation results in a continuity matrix, ${C}$, of the form

\begin{eqnarray}
{C[i,j]} = \begin{cases}
1,   & if (i,j) \in I_1 \times I_0  \nonumber \\
-1,  & if (i,j) \in I_1 \times I_1  \nonumber \\
0,   & else                 
\end{cases}
\end{eqnarray}

Thus, the extended matrix ${K_e'}={K_e}+{C}$ is of the form:
\begin{eqnarray}
{K_e'[i,j]} = \begin{cases}
0,                & if (i,j) \in I_0 \times I_1  \nonumber \\ 
1,                & if (i,j) \in I_1 \times I_0  \nonumber \\
-1,               & if (i,j) \in I_1 \times I_1  \nonumber \\
{K_e},     & else                 
\end{cases}
\end{eqnarray}

The new system is then
\begin{eqnarray}
({K_i' + K_e'}) \Phi_e &=& {K_i'} V_m \\ \nonumber
{M_i} I_m &=& - {K_i'} V_m
\end{eqnarray}

To ensure that the first equation is consistent, the elements of the right hand side in the index set 
$I_1$ are set to 0. 
% 
The new formulation was implemented in CARP \cite{Vigmond2003a}.

To test the new formulation, two experiments were performed on a cubic 0.6 $\times$ 0.6 $\times$ 0.6 $cm^3$ tetrahedral mesh:
1) propagation following a transmembrane stimulus and 2) shock-induced response. 
For the propagation experiment the monodomain model was used. A stimulus current 
of 250 $\mu$A/$cm^2$ was applied for 2 ms at the entire left face of the cube (Figure \ref{fig:robinBC_propagation}). 
In the shock experiment the bidomain was used. The shock was applied by placing the cathode at the left face of the cube 
and the anode, at the right face of the cube (Figure \ref{fig:robinBC_shock}). A shock intensity of 3 V was applied, resulting
in the electrical field of 5V/cm. In the two experiments, 
the boundary conductivity $\alpha$ was set to values ranging from 0.0 (fully decouple) to 10.0 (fully coupled). 

% \section{Results}
% 
% \subsection{Propagation with monodomain}
Figure \ref{fig:robinBC_propagation} shows propagation results using the new monodomain formulation. Results are shown for 
$\alpha =$ 0.0, 0.5, 1.0, and 10.0 $S/cm^2$. It is clear that for $\alpha =$ 0.0 $S/cm^2$ (top panels), propagation across
the boundary is fully blocked. As $\alpha$ is increased, propagation is gradually reestablished, until the boundary
is fully recoupled with $\alpha =$ 10.0 $S/cm^2$.


\begin{figure}[h]
	\centering
	\includegraphics[scale=1.0]{./figures/robinBC_propagation.png}
	% sim.pdf: 792x612 pixel, 72dpi, 27.94x21.59 cm, bb=0 0 792 612
	\caption{Propagation using the extended discontinuous FE approach for the monodomain model. From top to bottom, 
		$\alpha$ varies from 0.0 (fully decoupled) to 10.0 (fully coupled). 
		As $\alpha$ increases, flux across the boundary layer is gradually reestablished, until the two sides are again fully coupled.}
	\label{fig:robinBC_propagation}
\end{figure}


% \subsection{Shock-induced polarization with bidomain}
Figure \ref{fig:robinBC_shock} shows results for shock-induced response using the new bidomain formulation. 
Results are shown for $\alpha =$ 0.0, 0.5, 1.0, and 10.0 $S/cm^2$. 
The strongest response (virtual electrode formation) is observed for $\alpha =$ 0.0 $S/cm^2$ (top panels), where the boundary is fully
decoupled. This is in agreement with previous results shown with the discontinuous finite
element approach \cite{Costa2014}. As $\alpha$ is increased, the response becomes gradually weaker, until the boundary
is fully recoupled with $\alpha =$ 10.0 $S/cm^2$ and no virtual electrodes appear.
Moreover, the results show that, the extracellular space remains continuous (see extracellular potential panels) while
the intracellular space is discontinuous, demonstrating that the bidomain implemenation is consistent.

	\begin{figure}[h]
		\centering
		\includegraphics[width=0.85\paperwidth]{./figures/robinBC_shock.png}
		% sim.pdf: 792x612 pixel, 72dpi, 27.94x21.59 cm, bb=0 0 792 612
		\caption{Shock response using the extended discontinuous FE approach for the bidomain model. From top to bottom, 
			$\alpha$ varies from 0.0 (fully decoupled) to 10.0 (fully coupled). 
			Right panels show the transmembrane potential and the left panels show the extracellular potential.
			The shock response decreases as $\alpha$ increases, but the relationship is nonlinear.}
		\label{fig:robinBC_shock}
	\end{figure}
	

% \section{Conclusion}

The preliminary results demonstrate that the implementation works well with both monodomain and bidomain models. 
The new formulation will be a valuable tool to study 
the electrophysiological impact of the interaction between fibroblasts and myocytes
in future studies.





\renewcommand*{\bibfont}{\small}

\bibliographystyle{spmpsci}      % mathematics and physical sciences
%\bibliographystyle{spphys}       % APS-like style for physics
\bibliography{robin}

\end{document}
